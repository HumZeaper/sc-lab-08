public class PersonTax implements Taxable{
	private String name;
	private double salary;
	public PersonTax(String name,double salary){
		this.name = name;
		this.salary = salary;
	}
	public double getTax() {
		if(salary <= 300000){return salary*0.05;}
		else {return (300000*0.05) + ((salary-300000)*0.1);}
	}
	public String getName(){
		return name;
	}
}
