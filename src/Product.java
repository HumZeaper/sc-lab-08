public class Product implements Taxable{
	private String productName;
	private double price;
	
	public Product (String productName,double price){
		this.productName = productName;
		this.price = price;
	}
	
	public double getTax() {
	return price*0.07;
	}
	
	public String getProName(){
		return productName;
	}
}