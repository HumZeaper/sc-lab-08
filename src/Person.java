public class Person implements Measurable {

	private String name;
	private int height;
	
	public Person(String name,int height){
		this.name = name;
		this.height = height;
	}
	
	public double getMeasure() {
		return height;
	}
	public String toString(){
		return name;
	}
	public String getName(){
		return name;
	}

}
